# Přehled změn

## [1.1.2] - 2023-07-21

### Opraveno
- V režimu pohledové kontroly nyní nejsou dostupné některé nástroje a spuštění kontrol
- Opravena tvorba definičních bodů ve 2D
- Opraveno stylovaní pro chybu s kódem 15 (Plocha s chybným ohraničením) a 16 (Kolize ploch)
- Opraveny chyby v designu dialogů
- Opravená chyba pro přiblížení obsahu mapy pomocí obdélníkového výběru

## [1.0.23] - 2023-05-31

### Opraveno
- Přichytávání pohledových funkcí
- Odebráno logo

## [1.0.22] - 2023-05-23

### Opraveno
- Klient již nepovolí smazání podrobného bodu s navázaným prvkem.
- Není povolena úprava podrobných bodů.
- Opraveno chování pohledových funkcí pro přiblížení a oddálení ve 3D.

## [1.0.21] - 2023-05-17

### Opraveno
- Vyhledávání vrstvy s diakritikou.
- Ovládání pohldu ve 3D již nevyžaduje kursor přímo nad prvkem.
- Přechod mezi 2D, 3D již nezpůsobuje náhodné oddálení.

## [1.0.20] - 2023-05-11

### Opraveno
- Zvýraznění aktivních kontrol
- Undo, redo
- Externí odhlášení z aplikace
- Desetinná místa souřadnic ve WGS 84
- Oznámení o ukončení pohledové funkce

## [1.0.19] - 2023-05-04

### Opraveno
- Dvojí slider v panelu vrstev
- Napojení na KeyCLoak
- Undo editace

Formát je založený na [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
Verzování projektu je založené na [Semantic Versioning](https://semver.org/spec/v2.0.0.html).