# #
# # Deployment license.
# #
version                   = 2023.0
productName               = LuciadRIA
copyright                 = Powered by Hexagon.
message                   = Powered by Hexagon.
licensee                  = NESS s.r.o.
licenseType               = deployment
optionalModules           = Geometry,Panoramic
serialNumber              = ca9000
key                       = 5488c590829888f3efd0a0ff2cad00cc8c5918b6fffbbe7deb7d752d2e94f68b1da68a45b37f63776523d9272bfcd4205ecb5c69912649d86dc94ad33dced21d
#
ADMIN/1-FileReference         = 144454
ADMIN/2-MaintenanceInfo       = 
ADMIN/3-CustomerAccount       = NESS s.r.o.
ADMIN/4-CustomerNumber        = Prospect
ADMIN/5-ProjectName           = Railway Infrastructure Administration
ADMIN/6-ProjectNumber         = 2022-94542
ADMIN/7-DispatcherName        = Railway Infrastructure Administration
ADMIN/8-GenerationInstruction = L1686322487441
ADMIN/9-LicenceDefinition     = LuciadRIA EUL (2)
ENTITLES/0-DISCLAIMER-0        = The entitlement information in this file is for information only
ENTITLES/0-DISCLAIMER-1        = Entitlements are determined by the agreements between Luciad and the Licensee in those agreements
ENTITLES/1-Product and Tier    = LuciadRIA Pro
ENTITLES/2-License             = 2 SEATS
